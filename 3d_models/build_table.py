#!/usr/bin/env python3
###############################################################################
#####                             Information                             #####
###############################################################################
'''
This script is provided under the Creative Commons Attribution-ShareAlike 4.0 
International (CC BY-SA 4.0) license. In short, you are welcome to use, share,
and modify this file as long as you provide attribution.

To use this script, you need to have three things installed/configure:
* Install Python 3
* Install OpenSCAD
* Add the folder you have OpenSCAD installed to to your PATH or environmental
  variables.

The generated files will be created in folder called "generated_models".

Good luck printing all of these....a full table is 118 cubes and 118 mounting
brackets. It will take you quite some time ;)

Enjoy,
Austin Steingrube, Oak600.com
'''

###############################################################################
#####                               IMPORTS                              #####
###############################################################################
import os
import csv

###############################################################################
#####                              FUNCTIONS                              #####
###############################################################################

#Creates to "-o" notation easily
def out_file(m_file_str):
    return ('-o ' + str(m_file_str) + ' ')

#Creates the definition notation easily
def set_var(m_var_str, m_var_val):
    return ('-D "' + str(m_var_str) + '=\\"' + str(m_var_val) + '\\"" ')



###############################################################################
#####                             SCRIPT CODE                             #####
###############################################################################
'''Get the directory of this python script and use it to find files we need'''
dir_loc = os.path.dirname(os.path.realpath(__file__))
print ("Changing active directory to " + dir_loc)
#os.system("cd " + dir_loc)
os.chdir(dir_loc)
csv_file = os.path.join(dir_loc, "../data/periodic_table.csv")
if not os.path.exists(dir_loc + '/generated_files'):
    os.mkdir(dir_loc + '/generated_files')

'''We want to read the csv file and parse it into commands that we can use later.
   What we *don't* want to do is leave the csv file open any longer than we need
   to as OpenSCAD is a little less that speedy.'''
print("Reading in the csv file....")
command_list = []
with open(csv_file) as f:
    csv_data = csv.DictReader(f)
    for row in csv_data:
        command = "openscad " + \
        out_file('generated_files/' + str.lower(row['name']) + '.stl') + \
        set_var("symbol_text",row['symbol'] ) + \
        set_var("number_text", row['num']) + \
        set_var("weight_text", row['weight']) + \
        "simple-box.scad"

        command_list.append(command)

'''This will generate meshes for each element in the csv file one at a time. It would
   be faster to try to run multiple commands at once (one per core), but this will
   do for now.'''
print ("Starting creation....\n")
for command in command_list:
    print(command)
    os.system(command)

print ("done.")
