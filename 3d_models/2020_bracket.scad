
/* This script was written by Austin Steingrube to generate
   3D meshes for an element table. To use this file, it is
   recommended that you change the size of the box, fonts,
   and any other cosmetic tweaks one time, save the file,
   and then run the script called build_table.py
   
   For those who have never used OpenSCAD before, press "F6"
   to compile and view the 3D model from within the OpenSCAD
   editor.
   */

// ***********************************************************
// *****              Params Set Once Only               *****
// ***********************************************************
size_of_extrusion = 20; //mm
size_of_hole = 5.8; //mm diamter, 5.8 is for an M5 screw

sidewall_thickness = 4; //mm
hole_surface_thickness = 4; //mm
back_surface_thickness = 4; //mm


// The "back" is the portion that will be glued to the box. The
// width needs to be at least 2x your support thickness plus the
// clearance needed for whatever hardware. The wider, the better
// (more surface area for the glue)
// 
// Likewise, the back length should be at minimum the size of
// the extrusion. The longer, the better (more surface area for
// the glue.
back_width = 40; //mm
back_length = 35; //mm

// The registration bumps are square pyramids. 
registration_bump_height = 3; //mm


// ***********************************************************
// *****                Calculated Values                *****
// ***********************************************************

p1x = size_of_extrusion + back_surface_thickness;


// ***********************************************************
// *****                     Modules                     *****
// ***********************************************************
module side_mod()
{
    
    //The "inner" box to scoop out
    side_points = [
    [0,   0],
    [p1x, 0],
    [p1x, hole_surface_thickness],
    [hole_surface_thickness, back_length],
    [0, back_length],
    ];
    
    linear_extrude(height=sidewall_thickness)
    {
        polygon(side_points);
    };
}

module square_pyramid_mod(size_mm)
{  
    /*
          1  |  2
         ----4----
          0  |  3
    */
    points = [
    [-size_mm, -size_mm, 0], //0
    [-size_mm,  size_mm, 0], //1
    [ size_mm,  size_mm, 0], //2
    [ size_mm, -size_mm, 0], //3
    [ 0,  0, size_mm]  //4
    ];
    
    faces = [
    [4, 0, 1],
    [2, 4, 1],
    [3, 4, 2],
    [0, 4, 3],
    [0, 3, 1],
    [1, 3, 2]
    ];
    
    polyhedron(points=points, faces=faces);
}


module bump_mod()
{
    translate([ 0, back_length/4, back_width / 4 ])
    {
        rotate([90,0,-90])
        {
            square_pyramid_mod(registration_bump_height);
        } 
    }
    
    translate([ 0, back_length/4, 3*back_width / 4 ])
    {
        rotate([90,0,-90])
        {
            square_pyramid_mod(registration_bump_height);
        } 
    }
    
    translate([ 0, 3*back_length/4, back_width / 4 ])
    {
        rotate([90,0,-90])
        {
            square_pyramid_mod(registration_bump_height);
        } 
    }
    
        translate([ 0, 3*back_length/4, 3*back_width / 4 ])
    {
        rotate([90,0,-90])
        {
            square_pyramid_mod(registration_bump_height);
        } 
    }
    
}

module bracket_mod()
{
    difference()
    {
        union()
        {
            side_mod();
            
            translate([0,0,back_width - sidewall_thickness])
            {
                side_mod();
            };
            
            cube([p1x, hole_surface_thickness, back_width]);
            
            cube([back_surface_thickness, back_length, back_width]);
        }
        
        // The hole
        translate([back_surface_thickness + (size_of_extrusion/2),
              hole_surface_thickness * 1.5, back_width/2]){
        rotate([90,90,0]){
            cylinder(r=(size_of_hole/2), 
                     h = hole_surface_thickness*3, 
                     $fn=20);
            }
        }
    }
}


// ***********************************************************
// *****                     Script                      *****
// ***********************************************************
union(){
    bracket_mod();
    bump_mod();
}



