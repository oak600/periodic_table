
/* This script was written by Austin Steingrube to generate
   3D meshes for an element table. To use this file, it is
   recommended that you change the size of the box, fonts,
   and any other cosmetic tweaks one time, save the file,
   and then run the script called build_table.py
   
   For those who have never used OpenSCAD before, press "F6"
   to compile and view the 3D model from within the OpenSCAD
   editor.
   */

// ***********************************************************
// *****             Params Changed By Script            *****
// ***********************************************************
symbol_text = "Og";
number_text = "118";
weight_text = "294.21";



// ***********************************************************
// *****              Params Set Once Only               *****
// ***********************************************************
outer_box_width = 100; //mm
outer_box_height = 100; //mm
outer_box_depth = 60; //mm

// The thickness of the box walls.
box_wall_thickness = 5; //mm
box_back_thickness = 5; //mm

// The thickness of the text
symbol_text_thickness = 4; //mm
small_text_fn = 50;
large_text_fn = 80;

//The thickness of the hold made for the LED strip piece
led_hole_depth = 3; //mm
led_hole_min_width = 10; //mm
led_hole_taper_angle = 60; //degrees
led_per_meter = 60; //units per meter
num_leds_per_box = 3;

//Wiring channels should be less than the wall thickness
wiring_channel_depth = 2;  //mm
bezel_side_width = 10; //mm

//Bracket mounting
extrusion_width = 20 + 1; //mm, make sure to add enough for tolerances
bracket_back_length = 35; //mm
bracket_back_width = 40; //mm
registration_bump_height = 3; //mm

// ***********************************************************
// *****                Calculated Values                *****
// ***********************************************************
//The "inner" box to scoop out
inner_box_width = outer_box_width - (2 * box_wall_thickness);
inner_box_height = outer_box_height - (2 * box_wall_thickness);
inner_box_depth = outer_box_depth - box_back_thickness;

symbol_text_size = inner_box_height / 3; //mm
number_text_size = inner_box_height / 9;

//The centerlines
center_vert = (outer_box_height / 2) - (symbol_text_size / 2);
center_hori =  outer_box_width / 2;

//LED Hole
led_hole_length = (1000 / led_per_meter) * (num_leds_per_box + 0.5);

wiring_channel_width = outer_box_depth - (2 * bezel_side_width); //mm

// ***********************************************************
// *****                     Modules                     *****
// ***********************************************************
module taper_channel_mod(min_length, min_width, thickness, taper_angle)
{
        //Trig is in degrees for OpenSCAD
    taper_width = tan(taper_angle) * thickness;

    //****1 = top of hole, ****2 = bottom of hole
    xmin1 = -min_length / 2;
    xmax1 = min_length / 2;
    xmin2 = xmin1 - taper_width;
    xmax2 = xmax1 + taper_width;

    y1 = led_hole_depth;
    y2 = 0;

    zmin1 = -min_width / 2;
    zmax1 = min_width / 2;
    zmin2 = zmin1 - taper_width;
    zmax2 = zmax1 + taper_width;

    //Create arrays with points and faces
    taper_points = [
    [ xmin2,  y2,  zmax2 ],  //0
    [ xmax2,  y2,  zmax2 ],  //1
    [ xmax2,  y2,  zmin2 ],  //2
    [ xmin2,  y2,  zmin2 ],  //3
    [ xmin1,  y1,  zmax1 ],  //4
    [ xmax1,  y1,  zmax1 ],  //5
    [ xmax1,  y1,  zmin1 ],  //6
    [ xmin1,  y1,  zmin1 ]]; //7

    taper_faces = [
    [0,1,2,3],  // bottom
    [4,5,1,0],  // front
    [7,6,5,4],  // top
    [5,6,2,1],  // right
    [6,7,3,2],  // back
    [7,4,0,3]]; // left

    polyhedron( taper_points, taper_faces );
}
module wire_routing_mod()
{
    // The holes between the top and the inside for LED wiring
    x_trans_1 = (outer_box_width/2) - (led_hole_length / 2);
    x_trans_2 = (outer_box_width/2) + (led_hole_length / 2) - led_hole_depth;
    y_trans = outer_box_height - led_hole_depth;
    z_trans = (inner_box_depth / 2) + box_back_thickness - (led_hole_min_width/2);
    
    //The top wiring channel
    top_x_1 = (-wiring_channel_width / 2) + (outer_box_width - led_hole_length)/2;
    wiring_channel_z_2 =  bezel_side_width + (wiring_channel_width / 2);
    translate([top_x_1,outer_box_height,wiring_channel_z_2])
    {
         rotate([0,90,180]){
        taper_channel_mod(wiring_channel_width, 
             wiring_channel_width, 
             wiring_channel_depth, 45);
        }
    }
    
    top_x_2 = (wiring_channel_width / 2) + (outer_box_width + led_hole_length)/2;
    wiring_channel_z_2 =  bezel_side_width + (wiring_channel_width / 2);
    translate([top_x_2,outer_box_height,wiring_channel_z_2])
    {
         rotate([0,90,180]){
        taper_channel_mod(wiring_channel_width, 
             wiring_channel_width, 
             wiring_channel_depth, 45);
        }
    }

    
    //The bottom wiring channel
    translate([outer_box_width /2, 0, wiring_channel_z_2])
    {
        taper_channel_mod(outer_box_width, 
         wiring_channel_width, 
         wiring_channel_depth, 45);
    }
    
    
    //The right wiring channel
    translate([outer_box_width+.01, outer_box_height/2, wiring_channel_z_2])
    {
        rotate([0,0,90])
        {
            taper_channel_mod(outer_box_width, 
             wiring_channel_width, 
             wiring_channel_depth, 45);
        }
    }
    
    //The left wiring channel
    translate([0, outer_box_height/2, wiring_channel_z_2])
    {
        rotate([0,0, -90])
        {
            taper_channel_mod(outer_box_width, 
             wiring_channel_width, 
             wiring_channel_depth, 45);
        }
    }
}


module square_pyramid_mod(size_mm)
{  
    /*
          1  |  2
         ----4----
          0  |  3
    */
    points = [
    [-size_mm, -size_mm, 0], //0
    [-size_mm,  size_mm, 0], //1
    [ size_mm,  size_mm, 0], //2
    [ size_mm, -size_mm, 0], //3
    [ 0,  0, size_mm]  //4
    ];
    
    faces = [
    [4, 0, 1],
    [2, 4, 1],
    [3, 4, 2],
    [0, 4, 3],
    [0, 3, 1],
    [1, 3, 2]
    ];
    
    polyhedron(points=points, faces=faces);
}

module led_cutout_mod()
{
    /* the main cutout */
    translate([(outer_box_width-led_hole_length)/2,
        outer_box_height - box_wall_thickness,
        outer_box_depth*.80])
    {
        rotate([0,90,0])
        {
            linear_extrude(height=led_hole_length)
            {
                polygon(points=[[0, 0],
                [led_hole_min_width, 0],
                [led_hole_min_width, box_wall_thickness]]);
            }
        }
    }
    
    /*wiring holes */
    translate([(outer_box_width-led_hole_length - box_wall_thickness)/2,outer_box_height - (box_wall_thickness/2),outer_box_depth*.80 - (led_hole_min_width/2)])
    {
        cube([box_wall_thickness, 
            box_wall_thickness, 
            led_hole_min_width], center = true);
    }
    
    translate([(outer_box_width+led_hole_length + box_wall_thickness)/2,outer_box_height - (box_wall_thickness/2),outer_box_depth*.80 - (led_hole_min_width/2)])
    {
        cube([box_wall_thickness, 
            box_wall_thickness, 
            led_hole_min_width], center = true);
    }
}

module bracket_mount_mod()
{
    translate([-bracket_back_length/4, -bracket_back_width / 4, 0])
    {
        square_pyramid_mod(registration_bump_height);
    }
    
    translate([-bracket_back_length/4, bracket_back_width / 4, 0])
    {
        square_pyramid_mod(registration_bump_height);
    }
    
    translate([bracket_back_length/4, bracket_back_width / 4, 0])
    {
        square_pyramid_mod(registration_bump_height);
    }
    
    translate([bracket_back_length/4, -bracket_back_width / 4, 0])
    {
        square_pyramid_mod(registration_bump_height);
    }
    
}


module box_frame_mod()
{
    difference(){ //the led cutout
        difference() // the main box
        {
            cube([outer_box_width,
                  outer_box_height,
                  outer_box_depth], 
                  center=false);
            
            translate([box_wall_thickness, 
                        box_wall_thickness, 
                        box_back_thickness]){ 
            cube([inner_box_width,
                  inner_box_height,
                  outer_box_depth], 
                  center=false);
                        }
        }
        
        led_cutout_mod();

    }
}

module symbol_text_mod()
{
    translate([center_hori, center_vert, box_back_thickness]){
        linear_extrude(height = symbol_text_thickness)
        {
            text(text = symbol_text, 
                 size = symbol_text_size, 
                 font = "Arial:style=Bold", 
                 halign = "center", 
                 valign = "baseline",
                 $fn = large_text_fn);
        }
    }
}
    
module number_text_mod()
{
    translate([2 * box_wall_thickness, 
               outer_box_height - (2 *box_wall_thickness), 
               box_back_thickness])
    {
        linear_extrude(height = symbol_text_thickness)
        {
           text(text = number_text, 
                size = number_text_size, 
                font = "Arial:style=Bold", 
                halign = "left", 
                valign = "top",
                $fn = small_text_fn); 
        }
    }
}

module weight_text_mod()
{
    translate([inner_box_width, 
               outer_box_height - (2 *box_wall_thickness), 
               box_back_thickness])
    {
        linear_extrude(height = symbol_text_thickness)
        {
           text(text = weight_text, 
                size = number_text_size, 
                font = "Arial:style=Bold", 
                halign = "right", 
                valign = "top",
                $fn = small_text_fn); 
        }
    }
}

module complete_box_mod()
{
    union()
    {
       box_frame_mod();
       symbol_text_mod(); 
       number_text_mod();
       weight_text_mod();
    }
}


// ***********************************************************
// *****                     Script                      *****
// ***********************************************************

difference(){
    complete_box_mod();
    wire_routing_mod();

    translate([outer_box_width/2, 
            (outer_box_height + extrusion_width + bracket_back_length)/2, 
             0])
    {
        bracket_mount_mod();
    }
    translate([outer_box_width/2, 
            (outer_box_height - extrusion_width - bracket_back_length)/2, 
             0])
    {
        bracket_mount_mod();
    }

}
