# Periodic Table Readme

**Important!** This an active work in progress. As such, expect to find things a bit 
messy, incomplete, and somewhat buggy. I'm doing my best to try to make things
nice, but hey....this is just a hobby project.

If you like the idea, and like to donate to the project, check out my Amazon list
that has elements, hardware, and other bits/bobs that I'm looking at getting. You
can also check out the progress on oak600.com or facebook page facebook.com/Oak600

===============================================================================

## License 
This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
http://creativecommons.org/licenses/by-nc-sa/4.0/

In other words....
You may:
* Copy and redistribute the material in any medium or format
* Remix, transform, and build upon the material

As long as you:
* Give appropriate credit, provide a link to the license, and indicate if changes were made. 
* Distribute your contributions under the same license as the original.

You may not: 
* Use these materials for commercial purposes (aka making money from my work).

If you'd like to make or sell something commercially that uses this content, please
feel free to reach out to me and we can work something out. I am open to reasonable
licensing costs for those who would like to sell my work.


## Dependencies
In order to build the 3D models yourself, you will need to have the following:
* OpenSCAD 2015.03 or higher
** Add the location of the executable to your PATH variable
* Python v3.7.2 or higher (older may work...you'll have to experiment)