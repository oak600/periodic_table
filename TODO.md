This is a list of planned changes/additions to the branch. This is mostly to help
me keep track of what I need to do to avoid doing a million commits to the branch
every time I work on things.

#Programming to-do's

## OpenSCAD Models
* Add lip to bottom of box to prevent samples from sliding off accidentally
* Make slots in bracket for fine adjustments intead of just hole
* Investigate the best way to add plexiglass to protect dangerous/valuable elements

## Model Building Script
* Change output file names to start with atomic number in %03d format
* Add in a call to build the bracket model

## Raspberry Pi script for addressable LED's
* Create data structure and initializer to store elements, relative LED addresses, various properties
* Interface with Adafruit Neopixel library and write test scripts and animations
* Write animations based on chemical properties of elements
** Try to make everything configurable...don't use hard-coded colors or rates when possible.

#Interfacing to-do's
* Investigate input methods for changing animation types (touch screen? rotary switch? buttons?)
* Expand CSV file with more chemical properties (melting point, nuclear size, electron configs, etc)
* Create schematic or wiring description
* Draw up wire routing plans, namely to make sure we can reach all of the elements
   without a mess. May need to adjust some of the boxes models to hide wiring on exposed sides.
   
#Misc
* Check to make sure the registration holes/bumps don't interfere with each other after printing
** Looking for contact between the bracket and the box
* Do glue stress test to make sure the current method can handle a couple kg per box
** Most samples will be less than 1/2kg, so a max weight of 2+kg should be sufficient


#Nice to have's
* See if there is a nice way to compile all of the generated models into a single model to get
  a nice render via a script (can do by hand if needed)